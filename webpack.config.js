const { shareAll, withModuleFederationPlugin } = require('@angular-architects/module-federation/webpack');

module.exports = withModuleFederationPlugin({

  name: 'mfe1',
  filename: 'remoteEntry.js',
  exposes: {
    './ModuleA': './src/app/modulea/modulea.module.ts',
    './ModuleB': './src/app/moduleb/moduleb.module.ts'
  },

  shared: {
    ...shareAll({ singleton: true, strictVersion: true, requiredVersion: 'auto' }),
  },

});
